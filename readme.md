A Jupyter Notebook about the performance benefits of a "Priority Grid" in 
comparison to a dumb linear scan for point lookup within a 
user-specified bounding box.

Based on the VGI Summer School 2017 workshop by Prof. Dr. Sabine Storandt.

The .ipynb is the canonical source, the .html and .py were simply 
exported/converted.
